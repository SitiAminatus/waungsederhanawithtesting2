/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WarungSederhana;

/**
 *
 * @author AMINA
 */
public class TelorDadar extends LaukDecorator {
    Makanan makanan;
    
    public TelorDadar(Makanan makanan){
        this.makanan = makanan;
    }
    
    public String getDescription(){
        return makanan.getDescription() + ", Telur";
    }
    
    public double cost(){
        return 2000 + makanan.cost();
    }
}
