/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WarungSederhana;

/**
 *
 * @author AMINA
 */
public class Ayam extends LaukDecorator {
    Makanan makanan;
    
    public Ayam(Makanan makanan){
        this.makanan = makanan;
    }
    
    public String getDescription(){
        return makanan.getDescription() + ", Ayam";
    }
    
    public double cost(){
        return 5000 + makanan.cost();
    }
}
