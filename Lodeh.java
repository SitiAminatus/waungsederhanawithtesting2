/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WarungSederhana;

import WarungSederhana.SayurDecorator;

/**
 *
 * @author AMINA
 */
public class Lodeh extends SayurDecorator {
    Makanan makanan;
    
    public Lodeh(Makanan makanan){
        this.makanan = makanan;
    }
    
    public String getDescription(){
        return makanan.getDescription() + ", Lodeh";
    }
    
    public double cost(){
        return 2000 + makanan.cost();
    }
}
