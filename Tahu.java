/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WarungSederhana;

/**
 *
 * @author AMINA
 */
public class Tahu extends LaukDecorator {
    Makanan makanan;
    
    public Tahu(Makanan makanan){
        this.makanan = makanan;
    }
    
    public String getDescription(){
        return makanan.getDescription() + ", Tahu";
    }
    
    public double cost(){
        return 500 + makanan.cost();
    }
}
