/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WarungSederhana;

/**
 *
 * @author AMINA
 */
public class Operasi {
    private double hasil;
    public double sethasil(int pilih, double bil1, double bil2){
        switch (pilih) {
            case 1:
                hasil = bil1 + bil2;
                break;
            case 2:
                hasil = bil1 - bil2;
                break;
            case 3:
                hasil = bil1 * bil2;
                break;
            case 4:
                hasil = bil1 / bil2;
                break;
            case 5:
                hasil = Math.sin(bil1);
                break;
            case 6:
                hasil = Math.cos(bil1);
                break;
            case 7:
                hasil = Math.tan(bil1);
                break;
            default:
                hasil=0;
        }
        return hasil;
    }
    
    //Constructor
    Operasi(double hasil){
        this.hasil = hasil;
    }
}
