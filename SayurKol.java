/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WarungSederhana;

import WarungSederhana.SayurDecorator;

/**
 *
 * @author AMINA
 */
public class SayurKol extends SayurDecorator {
    Makanan makanan;
    
    public SayurKol(Makanan makanan){
        this.makanan = makanan;
    }
    
    public String getDescription(){
        return makanan.getDescription() + ", Kangkung";
    }
    
    public double cost(){
        return 1000 + makanan.cost();
    }
}
